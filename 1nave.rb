class Ship
    attr_accessor :ship, :ant, :pos, :exploded, :score
    def initialize
        @ship = ' '.colorize(:background => :white)
        @ant = '*'.colorize(:background => :blue)
        @pos = 10
        @exploded = false
        @score = 0
    end 
end
