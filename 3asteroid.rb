require 'colorize'
require_relative '2tablero'

class Asteroid

    attr_accessor :unshiftings, :size, :velocity, :position, :array, :value, :asteroids, :ast_good, :ast_index, :points, :scored

    @@asteroids = []

    def self.popasteroids
        @@asteroids.pop
    end

    def self.asteroids
        @@asteroids
    end

    def initialize(size, position)
        @size = size
        @points = (size*10)-10
        @scored = false
        @position = position
        @array = []
        size.times do |i|
            array[i] = []
            size.times do |j|
                array[i][j] = {:y => i - size + 1, :x => position + j}
            end
        end
        @ast_good = false
        @velocity = 1
        @value = 'O'.colorize(:black).colorize(:background => :black) 
        @@asteroids << self
        @ast_index = @@asteroids.index(self)
    end
end


