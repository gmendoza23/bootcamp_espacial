require 'colorize'
require_relative '1nave'
require_relative '3asteroid'

class Table
    attr_accessor :matrix, :color

    def initialize(a,b)
        @color = '*'.colorize(:background => :blue)
        @matrix = Array.new(a) {Array.new(b)}
        matrix.map! {|element| element.map {color}}
        
    end

    def table_show
         system("clear")
        matrix.each {|element| puts element.join('')}
    end

    def mod_table_ship(pos, value)                  #para imprimir la nave en el tablero
        @matrix[matrix.size - 1][pos] = value
    end

    def move_ship(dir, pos, actual, ant)            #movemos la nave con cada input
        if dir == 'a'
            mod_table_ship(pos, actual)
            mod_table_ship(pos + 1, ant)
        elsif dir == 'd'
            mod_table_ship(pos, actual)
            mod_table_ship(pos - 1, ant)
        end
    end

    def mod_table_asteroid(asteroid)                #introducimos los asteroides en la matriz, revisando si entran y si no se solapan
        (asteroid.size).times do |i|
            if asteroid.array[i][0][:y]>=0
                (asteroid.size).times do |j|
                    if @matrix[asteroid.array[i][j][:y]][asteroid.array[i][j][:x]] == asteroid.value
                        (j).times do |k|
                            @matrix[asteroid.array[i][k][:y]][asteroid.array[i][k][:x]] = color
                        end
                        asteroid.ast_good = false
                        break
                    else
                        @matrix[asteroid.array[i][j][:y]][asteroid.array[i][j][:x]] = asteroid.value
                    end
                end
            end
        end
    end

    def mod_asteroids(asteroid,ship)                #imprimimos en el tablero los asteroides a la hora de moverlos
        (asteroid.size).times do |i|
            if asteroid.array[i][0][:y]>=0 and asteroid.array[i][0][:y]<=14
                (asteroid.size).times do |j|
                    if @matrix[asteroid.array[i][j][:y]][asteroid.array[i][j][:x]] == ship.ship         #revisamos si un asteroide chocó con la nave
                        return true
                    end
                    @matrix[asteroid.array[i][j][:y]][asteroid.array[i][j][:x]] = asteroid.value
                #end
                    if i == 0 and asteroid.array[0][0][:y] >= 0                                         #reescribimos el tablero cuando ya el asteroide no está en esas casillas
                        if @matrix[asteroid.array[0][j][:y]-1][asteroid.array[0][j][:x]] == ship.ship
                            @matrix[asteroid.array[0][j][:y]-1][asteroid.array[0][j][:x]] = ship.ship
                        else
                            @matrix[asteroid.array[0][j][:y]-1][asteroid.array[0][j][:x]] = color
                        end
                        #end
                    end
                    if asteroid.array[0][0][:y] >= 13                                   #reescribimos el fondo del tablero
                        @matrix[asteroid.array[0][j][:y]][asteroid.array[0][j][:x]] = color
                    end
                    if (j == 0 || j == asteroid.size-1) and !(asteroid.scored)
                        if @matrix[asteroid.array[i][j][:y]][asteroid.array[i][j][:x]+1] == ship.ship ||
                            @matrix[asteroid.array[0][j][:y]][asteroid.array[0][j][:x]-1] == ship.ship
                            ship.score+=asteroid.points
                            asteroid.scored = true
                        end
                    end
                end
            end
        end
        false
    end

    def move_asteroids(ship)                #le aumentamos 1 a cada keyword :y de cada casilla de cada asteroide
        Asteroid.asteroids.length.times do |z|
            (Asteroid.asteroids[z].size).times do |i|
                (Asteroid.asteroids[z].size).times do |j|
                    Asteroid.asteroids[z].array[i][j][:y]+=1
                end
            end
            if (mod_asteroids(Asteroid.asteroids[z],ship))
                ship.exploded = true
            end
        end
    end
end