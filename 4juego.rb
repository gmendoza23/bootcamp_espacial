require_relative '1nave'
require_relative '2tablero'
require_relative '3asteroid'
require_relative 'utils'

class Game
    def self.start_game
        game = Game.new
        puts "LANZATE AHI MANO".colorize(:light_blue)
        sleep 0.5
        puts">DALE ENTER".colorize(:light_green)
        gets.chomp
        game.run
    end
    
    def run
        ship = Ship.new #instanciamos la nave y el tablero 15x20
        table = Table.new(15,20)
        table.mod_table_ship(ship.pos, ship.ship) #insertamos la nave en el tablero
        loop do
             if rand(0..10) <= 2 #frecuencia con la cual salen asteroides nuevos
                 loop do
                    asteroid = Asteroid.new(rand(2..4),rand(0..19)) #creamos un asteroide nuevo con tamaño y posicion random
                    if (asteroid.position)+asteroid.size-1 <= 19 #revisamos si el asteroide con ese tamaño entra en el tablero
                        asteroid.ast_good = true                #si ast_good es true, el asteroide nos sirve y no lo borramos
                        table.mod_table_asteroid(asteroid)      #insertamos el asteroide en el tablero
                        if !(asteroid.ast_good)
                            Asteroid.popasteroids               #si no entra en el tablero despues de haber revisado ast_good,
                        end                                     #lo quitamos del arreglo de asteroides de la clase
                    else
                        Asteroid.popasteroids                   
                    end
                break if asteroid.ast_good == true              #dejamos de probar asteroides si el actual entra en el tablero
                end
            end
            if !(Asteroid.asteroids.empty?)                     #esto nos sirve para correr el juego con el caso de que los asteroides aún no existan
                table.move_asteroids(ship)
                if ship.exploded == true
                    game_over
                end
            end
            @frames += 1
            handle_input(ship, table)
            table.table_show
            sleep @speed / @fps
            if @frames >= 600 #10 frames por segundo * 60 segundos = 600 frame
                game_won(ship.score)
            end
            @speed-=0.0001
        end
    end

      def initialize
        @frames = 0
        @fps = 10
        @speed = 1.0000
      end

    def handle_input(ship, table)                               #movimiento de la nave
        key = Utils.get_key&.chr
        case key
        when 'a'
        #mover :left
            if ship.pos > 0
                ship.pos-=1
                table.move_ship(key, ship.pos, ship.ship, ship.ant)
            end
        when 'd'
        #mover :right
            if ship.pos < 19
                ship.pos += 1
                table.move_ship(key, ship.pos, ship.ship, ship.ant)
            end
        end
    end

    def game_over
         system 'clear'
         print "."
         sleep 1
         system 'clear'
         print ".."
         sleep 1
         system 'clear'
         print "..."
         sleep 1
         puts "No se supo mas de la tripulacion".colorize(:red)
        raise StopIteration
    end

    def game_won(score)
        system 'clear'
        puts "Victoria!".colorize(:light_green)
        puts
        puts "Puntos: #{score}".colorize(:light_green)
       raise StopIteration
   end
end